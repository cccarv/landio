<?php
// Check if post is private 
if ( post_password_required() ) {
	return;
}
?>
    <div class="divider"></div>
	<div id="comments" class="comments-area">

		<?php if ( have_comments() ) : ?>
			<ol class="comment-list">
				<?php
					wp_list_comments( array(
						'style'      	=> 'ol',
						'per_page'	 	=> 999999, // TODO: should be a better solution
						'avatar_size' 	=> '32',
						'reply_text'	=>  '<span>' . __('پاسخ') . '</span>',
					) );
				?>
			</ol><!-- .comment-list -->
		<?php endif; // Check for have_comments(). ?>
		
		
			<?php

				$fields =  array(
					'author' => '<p class=" form-group comment-form-author"><input id="author" name="author" type="text" value="' . $comment_author . '" ' . ' placeholder="' . __( 'نام *' ) . '"/></p>',
					'email'  => '<p class=" form-group comment-form-email"><input id="email" name="email" type="text" value="' . $comment_author_email . '" ' . ' placeholder="' . __( 'ایمیل *' ) . '"/></p>',
					'url'    => '<p class=" form-group comment-form-url"><input id="url" name="url" type="text" value="' . $comment_author_url . '" placeholder="' . __( 'وبسایت' ) . '"/></p>',
				);
				?>
				<?php comment_form( array(
					'fields'						=> apply_filters( 'comment_form_default_fields', $fields ), 
					'logged_in_as' 			=> '<p class="logged-in-as">' . sprintf( __( 'وارد شده به اسم :  <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">مایلید خارج شوید؟</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
					'title_reply' 			=> __( 'پاسخ بدهید' ),
					'title_reply_to' 		=> __( 'پاسخ دادن به %s'),
					'cancel_reply_link'	=> __( 'کنسل کردن'),
					'label_submit' 			=> __( 'ارسال نظر' ),
					'comment_field' 		=> '<p class="form-group comment-form-comment"><label for="comment"><span class="screen-reader-text"></span></label><textarea id="comment" name="comment" rows="4" aria-required="true" placeholder="' . __( 'نظر شما؟' ) . '"></textarea></p>',
				));
			?>
	</div><!-- #comments -->